
# fluxation

[![NPM version][npm-image]][npm-url]
[![Latest tag][github-tag]][github-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]

A flux store and dispatcher which acts like an event emitter.

Unlike other flux implementations,
this library does not handle "actions",
so you are expected to create your own actions that dispatch events.
The philosophy for this is that each action should really just be a "module",
not some ridiculously complicated Component.

Create a flux instance:

```js
import Flux from 'fluxation'

export const flux = new Flux()
```

Pipe specific stores to your React instances:

```js
import React from 'react'

import flux from './flux'

export const View = React.createClass({
  getInitialState() {
    return {
      // initial state; fluxation does not handle schemas
      things: flux.store.things || [],
    }
  },

  componentDidMount() {
    // will pipe changes to `flux.store.things` to `this.state.things`
    flux.pipe(this, [
      'things',
    ])
  },

  render() {
    return (
      <div>{
        this.things.map(thing => <div>{thing.name}</div>)
      }</div>
    )
  }
})
```

Update things:

```js
export function updateThings() {
  fetch('/things').then(function (response) {
    return response.json()
  }).then(function (things) {
    flux.set('things', things)
  })
}

updateThings()
```

## Notes

Does not support ES6 Component syntax as it does not support [.isMounted()](http://facebook.github.io/react/docs/component-api.html#ismounted).

## API

### flux = new Flux()

Create a new flux instance.

### flux.store{}

The store for all the objects.
This supports dot attributes like MongoDB.
For example, if you set a dot attribute:

```js
flux.set('something.attribute', {
  name: 'lkjasdf'
})
```

Then it will be stored as:

```js
flux.store.something.attribute = {
  name: 'lkjasdf'
}
```

### flux.get(key)

Get a value from the store, supporting dot attributes.
Exists just for symmetry with `.set()`.

```js
const things = flux.get('things')
const thing = flux.get('things.0')
```

### flux.set(key, value)

Set a value from the store, supporting dot attributes.
When you set a key, an event with the same key will be emitted.
This is how components will know whether to update.

```js
// creates an event called `things`
flux.set('settings', {}) // emits `settings`
flux.set('settings.preferences', { // emits `settings.preferences`
  name: 'something new'
})
```

Note that updates are not sent for objects other than those you specifically set.
In other words, the last function call above will emit `settings.preferences` events,
but not `settings`.
You will have to `flux.trigger('settings')` manually.

You can use `.set()` in two ways.

1. Set the object as a whole:

```js
const settings = flux.get('settings')
settings.asdf = 1
flux.set('settings', settings)
```

2. Set each property specifically:

```js
flux.set('settings.some_setting', true)
```

The first method is harder to dispatch events,
but the second method is harder to listen to events.
The second method is more specifically and probably more performant.

### flux.pipe(component, events)

Pipe events to the component.
For example, if you `flux.set('things', [])`,
then flux will do:

```js
component.setState({
  things: things,
})
```

This is for data that is passed from the store,
not from props.

`events` could also be an object of the form:

```js
{
  <local state property>: <flux store property>,
  <local state property>: true // uses the same property name for both
}
```

### flux.onUpdate(this, namespace, object)

Updates the current component when an object is updated.
The `namespace` is the namespace for the object (i.e. `things` or `users`).
Namespacing objects improves performance and describes what type of object it is.

The `object` is the actual object instance, which should match exactly.
This is for items that are passed as props but updated through the store.

For example, you may update something like this:

```js
const thing = flux.store.things.find(thing => thing.name === 'Taylor Swift')
thing.name = 'Taylor Alison Swift'
// marks `thing` as updated
flux.update('things', thing)

// then listen for it
export const Thing = React.createClass({
  componentDidMount() {
    flux.onUpdate(this, 'things', this.props.thing)
  }
})
```

The flux will tell the component to rerender by triggering:

```js
setState({
  _random: Date.now()
})
```

> This is a hack to force a re-render as .forceUpdate() is borked.

### flux.update(namespace, object)

Mark an object as updated.

[npm-image]: https://img.shields.io/npm/v/fluxation.svg?style=flat-square
[npm-url]: https://npmjs.org/package/fluxation
[github-tag]: http://img.shields.io/github/tag/jonathanong/fluxation.svg?style=flat-square
[github-url]: https://github.com/jonathanong/fluxation/tags
[travis-image]: https://img.shields.io/travis/jonathanong/fluxation.svg?style=flat-square
[travis-url]: https://travis-ci.org/jonathanong/fluxation
[coveralls-image]: https://img.shields.io/coveralls/jonathanong/fluxation.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/jonathanong/fluxation
[david-image]: http://img.shields.io/david/jonathanong/fluxation.svg?style=flat-square
[david-url]: https://david-dm.org/jonathanong/fluxation
[license-image]: http://img.shields.io/npm/l/fluxation.svg?style=flat-square
[license-url]: LICENSE
[downloads-image]: http://img.shields.io/npm/dm/fluxation.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/fluxation
