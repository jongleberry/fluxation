'use strict'

const assert = require('assert')
const React = require('react')

const Flux = require('..')

describe('.get() and .set()', function () {
  const flux = new Flux()

  describe('.set()', function () {
    it('should set the store value', function () {
      flux.set('asdf.asdf', 1)
      assert.equal(flux.store.asdf.asdf, 1)
    })

    it('should emit an event', function (done) {
      flux.once('set:asdf.asdf', function (val) {
        assert.equal(val, 2)
        done()
      })
      flux.set('asdf.asdf', 2)
    })
  })

  describe('.get()', function () {
    it('should get the value', function () {
      assert.equal(2, flux.get('asdf.asdf'))
    })
  })
})

describe('.update() and .onUpdate()', function () {
  const flux = new Flux()
  const obj = {}

  it('should not trigger .onUpdate() if .update()', function (done) {
    flux.onUpdate({
      setState: function (obj) {
        assert(obj._random)
        done()
      },
      isMounted: function () {
        return true
      },
    }, 'things', obj)

    flux.update('things', obj)
  })
})
