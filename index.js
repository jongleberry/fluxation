'use strict'

var Emitter = require('component-emitter')
var dot = require('dot-prop')

module.exports = Flux

function Flux() {
  if (!(this instanceof Flux)) return new Flux()

  Emitter(this)

  // TODO: use Map()
  this.store = Object.create(null)
}

/**
 * @param {String} key
 */

Flux.prototype.get = function (key) {
  if (typeof key !== 'string') throw new TypeError('key must be a string!')
  return dot.get(this.store, key)
}

/**
 * @param {String} key
 * @param {Mixed} value
 */

Flux.prototype.set = function (key, value) {
  if (typeof key !== 'string') throw new TypeError('key must be a string!')
  dot.set(this.store, key, value)
  this.emit('set:' + key, value)
  return this
}

/**
 * @param {React.Component} component
 * @param {Array|Object|Map} events
 */

Flux.prototype.pipe = function (component, events) {
  if (typeof namespace !== 'object') throw new TypeError('events must be an object or an array!')
  // convert an array to an object
  if (Array.isArray(events)) {
    var _events = {}
    Object.keys(events).forEach(function (name) {
      _events[name] = name
    })
    events = _events
  }
  // parse `true`s
  Object.keys(events).forEach(function (name) {
    if (events[name] === true) events[name] = name
  })
  Object.keys(events).forEach(function (name) {
    var key = events[name]
    var event = 'set:' + key
    this.on(event, function listener(value) {
      if (!component.isMounted()) return this.off(event, listener)
      var stateChange = {}
      stateChange[name] = value
      component.setState(stateChange)
    })
  }, this)
  return this
}

/**
 * @param {React.Component} component
 * @param {String} namespace
 * @param {Mixed} object
 */

Flux.prototype.onUpdate = function (component, namespace, object) {
  if (typeof namespace !== 'string') throw new TypeError('namespace must be a string!')
  var event = 'update:' + namespace
  this.on(event, function listener(obj) {
    // stop listening if unmounted
    if (!component.isMounted()) return this.off(event, obj)
    // don't continue if it's not the same object
    if (obj !== object) return
    // set state to trigger a re-render
    component.setState({
      _random: Math.random(),
    })
  })
  return this
}

/**
 * @param {String} namespace
 * @param {Mixed} object
 */

Flux.prototype.update = function (namespace, object) {
  if (typeof namespace !== 'string') throw new TypeError('namespace must be a string!')
  this.emit('update:' + namespace, object)
  return this
}
